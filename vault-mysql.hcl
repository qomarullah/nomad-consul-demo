https://www.vaultproject.io/docs/secrets/databases/mysql-maria

export SERVER_1_IP=139.59.117.162
export SERVER_2_IP=165.22.57.225
export SERVER_3_IP=165.22.57.224

export VAULT_ADDR=http://localhost:8200
export VAULT_TOKEN=s.MdSH8InU1Qurw8BJrhx8Kjol

vault
root/token=s.MdSH8InU1Qurw8BJrhx8Kjol
key1=/phrcstuSOMkg5GbmmgRbczpMIvBsPdjFc2A7iB2468=



vault secrets enable database
vault write database/config/database-test1 \
    plugin_name=mysql-database-plugin \
    connection_url="root:aplikasi@tcp(165.22.57.224:3309)/" \
    allowed_roles="my-role1" \


vault write database/roles/my-role1 \
    db_name=database-test1 \
    creation_statements="CREATE USER '{{name}}'@'%' IDENTIFIED BY '{{password}}';GRANT SELECT ON *.* TO '{{name}}'@'%';" \
    default_ttl="1h" \
    max_ttl="24h"

    
    
Success! Data written to: database/roles/my-role


vault read database/creds/my-role1

root@app1:~# vault read database/creds/my-role1
Key                Value
---                -----
lease_id           database/creds/my-role1/Qx2ehb7rS30fSvglEwhXLydk
lease_duration     1h
lease_renewable    true
password           s4z-iPuXEbmsjwcKOWgZ
username           v-root-my-role1-vaJfC8SLILItrkrS

root@app1:~# vault read database/creds/my-role1
Key                Value
---                -----
lease_id           database/creds/my-role1/vfRswLm39CA7dFwwNnPjzHGL
lease_duration     1h
lease_renewable    true
password           DLJA12SaQI68bVs-PKvi
username           v-root-my-role1-eIPPqANCeSymAtmG



job "webapi" {
  datacenters = ["dc1"]
  group "webapi" {
	count = 1
    network {
      port "http" {
        static = "10000"
		to = "8090"
      }
    }
	service {
		name="webapi"
	}
    task "server" {
        vault {
            policies = ["nomad-server"]
        }
        

        template {
            data = <<EOH
            LOG_LEVEL="{{key "service/webapi/log-level"}}"
            API_KEY="{{with secret "kvttl/webapi"}}{{.Data.api_key}}{{end}}"
            DB_USERNAME =  "{{with secret "database/creds/my-role1"}}{{.Data.username}}{{end}}"
            DB_PASSWORD =  "{{with secret "database/creds/my-role1"}}{{.Data.password | toJSON }}{{end}}"
            EOH
            destination = "secrets/file.env"
            env         = true
        }

	   env {
		NODE = "${attr.unique.hostname}"
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
      }
	  driver = "docker"
	  config {
		image = "docker.io/qomarullah/go-server:v0.1.2"
		ports = ["http"]
		auth {
		  username = "qomarullah"
		  password = "qazwsx"
		}
	  }
	 
  	}
  }
}