job "webapi" {
  datacenters = ["dc1"]
  group "webapi" {
	count = 1
    network {
      port "http" {
        static = "10000"
		to = "9000"
      }
    }
	service {
		name="webapi"
		tags = ["fw-esb", "fw-smsgw"]
	}
    task "server" {
	   env {
		NODE = "${attr.unique.hostname}"
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
        DBC = "trust"
        NODE_ENV = "dev"
        CONFIG_FILE = "local/file.env"
      }

      artifact {
        source = "http://localhost/assets/file.env"
      }
     template {
        source        = "local/file.env"
        destination   = "new/file.env"
        change_mode   = "restart"
        env = true
      }

	  driver = "docker"
	  config {
		image = "localhost:5000/telkomsel/web-api"
        ports = ["http"]
		auth {
		  username = "qomarullah"
		  password = "qazwsx"
		}
	  }
	
  	}
  }
}
