

job "webapi" {
  datacenters = ["dc1"]
  group "webapi" {
	count = 1
    network {
      port "http" {
        static = "10000"
		to = "9000"
      }
    }
	service {
		name="webapi"
		tags = ["fw-esb", "fw-smsgw"]
	}
    task "server" {
	   env {
		NODE = "${attr.unique.hostname}"
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
        DBC = "trust"
        NODE_ENV = "dev"
        CONFIG_FILE = "local/file.env"
      }

      artifact {
        source = "http://localhost/assets/file.env"
        destination = "local/file.env"
      }
    
     //template {
       // source        = "local/file.env"
       // destination   = "new/file.env"
       // change_mode   = "restart"
       // env = true
     // }

	  driver = "docker"
	  config {
		image = "localhost:5000/telkomsel/web-api"
        ports = ["http"]
		auth {
		  username = "qomarullah"
		  password = "qazwsx"
		}
	  }
	
  	}
  }
}



job "webapi" {
  datacenters = ["dc1"]
  group "webapi" {
	count = 1
    network {
      port "http" {
        static = "10000"
		to = "9000"
      }
    }
	service {
		name="webapi"
		tags = ["fw-esb", "fw-smsgw"]
	}
    task "server" {
	   env {
		NODE = "${attr.unique.hostname}"
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
        DBC = "trust"
        NODE_ENV = "dev"
        CONFIG_FILE = "local/file.env"
      }

      artifact {
        source = "http://localhost/assets/file.env"
        destination = "local/file.env"
      }
    
      vault {
              policies = ["nomad-server"]
        }
        

        template {
            data = <<EOH
            ESB_SERVER="{{with secret "kvttl/webapi"}}{{.Data.api_key}}{{end}}"
            EOH
            destination = "secrets/file.env"
            change_mode   = "restart"
            env         = true
        }

	  driver = "docker"
	  config {
		image = "localhost:5000/telkomsel/web-api"
        ports = ["http"]
		auth {
		  username = "qomarullah"
		  password = "qazwsx"
		}
	  }
	
  	}
  }
}





job "webapi" {
  datacenters = ["dc1"]
  group "webapi" {
	count = 1
    network {
      port "http" {
        static = "10000"
		to = "9000"
      }
    }
	service {
		name="webapi"
		tags = ["fw-esb", "fw-smsgw"]
	}
    task "server" {
	   env {
		NODE = "${attr.unique.hostname}"
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
        DBC = "trust"
        NODE_ENV = "dev"
        CONFIG_FILE = "local/file.env"
      }

     
     vault {
            policies = ["nomad-server"]
      }
      
     artifact {
        source = "http://localhost/assets/file.env.tpl"
        destination = "local/file.env.tpl"
      }
    
      template {
        source      = "local/file.env.tpl"
        destination = "local/file.env"
      }


	  driver = "docker"
	  config {
		image = "localhost:5000/telkomsel/web-api"
        ports = ["http"]
		auth {
		  username = "qomarullah"
		  password = "qazwsx"
		}
	  }
	
  	}
  }
}



#docker local



ESB_SERVER=test DBC=trust NODE_ENV=dev node server.js  
DBC=trust NODE_ENV=dev FILE_CONFIG=.env node server.js



https://docs.docker.com/registry/deploying/

docker run -d -p 5000:5000 --restart=always --name registry registry:2
docker build -t telkomsel/web-api .

docker tag telkomsel/web-api localhost:5000/telkomsel/web-api
docker push localhost:5000/telkomsel/web-api
docker run -it -p 9001:9000 -v localhost:5000/telkomsel/web-api 



docker inspect 580c37406ad5 | jq '.[0].HostConfig.Binds'
docker exec -it a7c84604d5a7 /bin/sh


