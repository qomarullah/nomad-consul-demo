job "service-abc" {
  datacenters = ["dc1"]
  group "service" {
	count = 1
    network {
	    mode = "bridge"
      port "http" {}
    }

	service {
		name="service-a"
		port = "http"
		connect {
			sidecar_service {
				 proxy {}
			}
		}
	}
	service {
		name="service-b"
		port = "http"
		connect {
			sidecar_service {
				 proxy {}
			}
		}
	
	}
	service {
		name="service-c"
		port = "http"
		connect {
			sidecar_service {
				 proxy {}
			}
		}
	
	}
    task "server" {
      env {
        NODE = "${attr.unique.hostname}"
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
      } 
      driver = "docker"
      config {
      image = "docker.io/qomarullah/go-server:v0.1.2"
      ports = ["http"]
      auth {
        username = "qomarullah"
        password = "qazwsx"
      }
	  }
	}
  }
}


job "webapi" {
  datacenters = ["dc1"]
  group "webapi" {
	count = 1
    network {
	   mode = "bridge"
     port "http" {
        static = 10000
      }
    }
	service {
		name="webapi"
		port = "http"
    connect {
			sidecar_service {
				proxy {
					upstreams {
						destination_name = "service-a"
						local_bind_port  = 9001
					}
				
					upstreams {
						destination_name = "service-b"
						local_bind_port  = 9002
					}
					upstreams {
						destination_name = "service-c"
						local_bind_port  = 9003
					}
				  upstreams {
						destination_name = "legacy"
						local_bind_port  = 9004
					}
          upstreams {
						destination_name = "legacy-mysql"
						local_bind_port  = 3306
					}
				
			}
		 }
		}
			
	}
	      
    task "server" {
      env {
        NODE = "${attr.unique.hostname}"
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
      } 
      driver = "docker"
      config {
      image = "docker.io/qomarullah/go-server:v0.1.3"
      ports = ["http"]
      auth {
        username = "qomarullah"
        password = "qazwsx"
      }
	  }
	}
  }
}



#ingress constraint node
job "ingress-webapi" {
  datacenters = ["dc1"]

  constraint {
    attribute    = "${meta.ingress}"
    set_contains = "group-a"
  }

  group "ingress-group" {
    network {
      mode = "bridge"
      port "inbound" {
        static = 10000
        to     = 10000
      }
    }

    service {
      name = "ingress-webapi"
      port = "10000"
      tags = [
			  "traefik.enable=true",
			  "traefik.http.routers.http.rule=PathPrefix(`/`)",
		  ]
      connect {
        gateway {
          proxy {
          }
          ingress {
            listener {
              port     = 10000
              protocol = "tcp"
              service {
                name = "webapi"
              }
            }
          }
        }
      }
    }
  }
}



#running legacy
docker run -d -e PORT=50000 -e IP=139.59.117.162 -p 50000:50000 qomarullah/go-server:v0.1.2

#register legacy service
{
  "id": "legacy",
  "name": "legacy",
  "address": "139.59.117.162",
  "port": 50000,
  "check": {
    "name": "ping check",
    "args": ["ping", "-c1", "139.59.117.162"],
    "interval": "30s",
    "status": "passing"
  }
}
curl --request PUT --data @external.json localhost:8500/v1/agent/service/register


#api gateway legacy mysql
job "api-gateway-legacy" {
  datacenters = ["dc1"]
  constraint {
    attribute    = "${meta.terminating}"
    set_contains = "group-a"
  }
  group "gateway" {
    network {
      mode = "bridge"
    }
    service {
      name = "api-gateway-legacy"
      connect {
        gateway {
          proxy {
          }
          terminating {
            service{ 
                name = "legacy-mysql"
              }
             }
          
        }
      }
    }
  }
}


#api gateway
job "api-gateway-legacy" {
  datacenters = ["dc1"]
  namespace ="digipos"
  constraint {
    attribute    = "${meta.terminating}"
    set_contains = "group-a"
  }
  group "gateway" {
    network {
      mode = "bridge"
    }
    
    service {
      name = "api-gateway-legacy"
      connect {
        gateway {
          proxy {
          }
          terminating {
            service{
              name = "*"
             }
          }
          
        }
      }
    }
  }
}

