#static port
#s.VFpKuJ1Fk6Vg0BNBmpnXe3Qb
job "test.example.com" {
  datacenters = ["dc1"]
  group "webapi" {
	count = 1
    network {
      port "http" {
        static = "10000"
		to = "8090"
      }
    }
	service {
		name="webapi"
	}
    task "server" {
	   env {
		NODE = "${attr.unique.hostname}"
        PORT = "${NOMAD_PORT_http}"
        IP   = "${NOMAD_IP_http}"
        VAULT_TOKEN = "s.VFpKuJ1Fk6Vg0BNBmpnXe3Qb"
        CERTIFICATE = "${NOMAD_SECRETS_DIR}"
      }
      template {
data = <<EOH
{{ with secret "pki_int/issue/example-dot-com" "common_name=test.example.com" "ttl=24h" "ip_sans=127.0.0.1" }}
{{- .Data.certificate -}}
{{ end }}
EOH
            destination   = "${NOMAD_SECRETS_DIR}/server.crt"
            change_mode   = "restart"
            }

       template {
data = <<EOH
{{ with secret "pki_int/issue/example-dot-com" "common_name=test.example.com" "ttl=24h" "ip_sans=127.0.0.1" }}
{{- .Data.private_key -}}
{{ end }}
EOH
          destination   = "${NOMAD_SECRETS_DIR}/server.key"
          change_mode   = "restart"
      }
    
	  driver = "docker"
	  config {
		image = "docker.io/qomarullah/go-server:v0.1.1"
		ports = ["http"]
		auth {
		  username = "qomarullah"
		  password = "qazwsx"
		}
	  }
	
  	}
  }
}